package net.minevn.fakeversion

import net.md_5.bungee.api.event.ProxyPingEvent
import net.md_5.bungee.api.plugin.Listener
import net.md_5.bungee.api.plugin.Plugin
import net.md_5.bungee.event.EventHandler

class FakeVersion: Plugin(), Listener {
	private val messages = listOf(
		"Miniflex ngu lắm",
		"Miniflex ngu như bò",
		"Miniflex tuổi loz",
		"Miniflex nhục như chó",
		"Miniflex non",
		"Miniflex đần độn",
		"Miniflex thiểu năng",
		"Miniflex Nguyễn Anh Đức là một thằng bại não",
	)

	override fun onEnable() {
		proxy.pluginManager.registerListener(this, this)
	}

	@EventHandler
	fun onPing(e: ProxyPingEvent) {
		e.response?.version?.name = messages.random()
	}
}
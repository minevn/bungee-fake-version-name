import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("java")
    kotlin("jvm") version "1.7.10"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    compileOnly("net.md-5:bungeecord-api:1.12-SNAPSHOT")
    compileOnly(kotlin("stdlib-jdk8"))
}

tasks {
    val jarName = "FakeVersion"
    var originalName = ""
    val path = project.properties["shadowPath"]

    jar {
        originalName = archiveFileName.get()
    }

    register("customCopy") {
        dependsOn(jar)

        doLast {
            if (path != null) {
                println("Copying $originalName to $path")
                val to = File("$path/$originalName")
                val rename = File("$path/$jarName.jar")
                File(project.projectDir, "build/libs/$originalName").copyTo(to, true)
                if (rename.exists()) rename.delete()
                to.renameTo(rename)
                println("Copied")
            }
        }
    }


    assemble {
        dependsOn(get("customCopy"))
    }
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}